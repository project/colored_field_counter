<?php

namespace Drupal\colored_field_counter\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Plugin\Field\FieldWidget\StringTextfieldWidget;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SimpleStringTextfieldWidget.
 *
 * Provides custom widget for textfield.
 * This widget add a counter below each textfield.
 * This counter informs contributors of recommended text sizes.
 * These recommendations are configurable for each textfield with this widget.
 *
 * @FieldWidget(
 *   id = "simple_string_textfield",
 *   label = @Translation("Textfield with colored counter"),
 *   field_types = {
 *     "string",
 *     "text",
 *   }
 * )
 */
class SimpleStringTextfieldWidget extends StringTextfieldWidget {

  use BaseSimpleTrait;

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $this->makeAttachement($element['value']);

    return $element;
  }

}
