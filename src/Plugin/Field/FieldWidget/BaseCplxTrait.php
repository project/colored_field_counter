<?php

namespace Drupal\colored_field_counter\Plugin\Field\FieldWidget;

use Drupal\Core\Form\FormStateInterface;
use Drupal\field\Entity\FieldConfig;

/**
 * Base Trait for all specific traits.
 *
 * @var Trait
 */
trait BaseCplxTrait {

  /**
   * Empty Conf data.
   *
   * @var array
   */
  protected $emptyConf = [['color' => '', 'low' => '' , 'max' => '']];

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'color_settings' => [
            [
              'color' => '',
              'low' => '',
              'max' => '',
            ],
      ],
      'optimal_size' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);

    if (!$this->getSetting('color_settings')) {
      $settings = [];
    }
    else {
      $settings = $this->getSetting('color_settings');
    }

    if (!$this->getSetting('optimal_size')) {
      $optimal_size = ($this->isLong === TRUE) ? 255 : $this->getSetting('size');
    }
    else {
      $optimal_size = $this->getSetting('optimal_size');
    }

    $element['optimal_size'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Content optimal size'),
      '#default_value' => $optimal_size ?? "",
      '#description' => $this->t('The size (in number of characters) for the optimal render'),
    ];

    $element['notice_wrapper'] = [
      '#type' => 'details',
      '#title' => $this->t('How to configure'),
    ];
    $element['notice_wrapper']['notice'] = [
      '#type' => 'processed_text',
      '#text' => $this->t("Limits can have two forms : \n - numerics : Absolut number of characters. \n Exemple : '50' means the limit are 50 characters\n - percentages : Relative limit to the Field max size (or optimal size for for long texts)  \n Exemple : for a text field limited to 100 characters, '10%' means the limit are to 90chararcters (Max - limit)"),
    ];

    $table = $this->buildTable($settings);
    $element['color_settings'] = $table;

    return $element;
  }

  /**
   * Build settings table (used in widgets).
   */
  public function buildTable($settings) {
    // Add the headers.
    $table = [
      '#type' => 'table',
      '#title' => 'Settings tabs',
      '#header' => [
        $this->t('Color'),
        $this->t('Low Limit'),
        $this->t('Max limit'),
      ],
    ];

    // Add input fields in table cells.
    for ($i = 0; $i < 3; $i++) {
      $table[$i]['color'] = [
        '#type' => 'select',
        '#title' => $this->t('Color'),
        '#title_display' => 'invisible',
        '#options' => [
          '' => $this->t('Select a color'),
          'green' => $this->t('Green'),
          'orange' => $this->t('Orange'),
          'red' => $this->t('Red'),
        ],
        '#default_value' => $settings[$i]['color'] ?? "",
      ];

      $table[$i]['low'] = [
        '#type' => 'textfield',
        '#title' => $this->t('low'),
        '#title_display' => 'invisible',
        '#default_value' => $settings[$i]['low'] ?? "",
      ];

      $table[$i]['max'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Max'),
        '#title_display' => 'invisible',
        '#default_value' => $settings[$i]['max'] ?? "",
      ];
    }

    $table[0]['low']['#disabled'] = TRUE;
    $table[0]['low']['#default_value'] = $this->t('Empty');
    $table[2]['max']['#disabled'] = TRUE;
    $table[2]['max']['#default_value'] = $this->t('Field Max size');

    return $table;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    $settings = $this->getSetting('color_settings');

    if ($settings == $this->emptyConf) {
      $summary[] = $this->t('The widget is not configured.');
    }
    else {

      $optimal_size = $this->getSetting('optimal_size');
      $max_size = $this->getMaxSize();

      $summary[] = $this->t('The optimal size is @optim characters', ['@optim' => $optimal_size]);
      $summary[] = $this->t('Color settings');
      foreach ($settings as $values) {

        $summary[] = $this->formatLine($values, $max_size);

      }
    }
    return $summary;
  }

  /**
   * Get field max size.
   *
   * @return int
   *   Size.
   */
  protected function getMaxSize() {
    $optimal_size = $this->getSetting('optimal_size');
    return ($this->isLong === TRUE) ? $optimal_size : 255;
  }

  /**
   * Format string for widget config summary.
   *
   * @param array $conditions
   *   Configuration.
   * @param int $max_size
   *   Max size of field.
   * @param int $prefix_level
   *   How many tabulation needed.
   *
   * @return mixed
   *   Translatable string.
   */
  protected function formatLine(array $conditions, $max_size, $prefix_level = 1) {

    $prefix = ' ' . str_repeat('- ', $prefix_level);

    if ($conditions['max'] == $this->t('Field Max size')->__toString() || $conditions['max'] == -1) {
      $max_string = NULL;
    }
    else {
      $max_string = $this->calculateString($conditions['max'], $max_size, TRUE);
    }

    if ($conditions['low'] == $this->t('Empty')->__toString() || $conditions['low'] == 0) {
      $low_string = NULL;
    }
    else {
      $low_string = $this->calculateString($conditions['low'], $max_size);
    }

    if ($low_string != NULL && $max_string != NULL) {
      return $prefix . $this->t('@color from @low up to @max', [
        '@color' => $conditions['color'],
        '@low' => $low_string,
        '@max' => $max_string,
      ]);
    }
    if ($low_string == NULL) {
      return $prefix . $this->t('@color up to @max', [
        '@color' => $conditions['color'],
        '@max' => $max_string,
      ]);
    }
    if ($max_string == NULL) {
      return $prefix . $this->t('@color from @low', [
        '@color' => $conditions['color'],
        '@low' => $low_string,
      ]);
    }

  }

  /**
   * Generate String information.
   *
   * @param string $value
   *   Field value.
   * @param string $max_size
   *   Field max size.
   * @param bool $floor
   *   How to round.
   *
   * @return mixed
   *   Translatable string.
   */
  protected function calculateString($value, $max_size, $floor = FALSE) {
    $string = $this->t('@nb char', ['@nb' => $this->getNbChar($value, $max_size, $floor)]);
    if (strstr($value, '%')) {
      $string = $this->t('@per (@nb char)',
      [
        '@nb' => $this->getNbChar($value, $max_size, $floor),
        '@per' => trim($value),
      ]);
    }
    return $string;
  }

  /**
   * Calculate number of char.
   *
   * @param string $value
   *   Field value.
   * @param string $max_size
   *   Field max size.
   * @param bool $floor
   *   How to round.
   *
   * @return int
   *   Number of char.
   */
  protected function getNbChar($value, $max_size, bool $floor = FALSE) {
    if (strstr($value, '%')) {
      $nb = str_replace('%', '', $value);
      $raw_nb_char = $max_size * trim($nb) / 100;
      $nb_char = ($floor === TRUE) ? round($raw_nb_char, 0, PHP_ROUND_HALF_DOWN) : round($raw_nb_char, 0, PHP_ROUND_HALF_UP);
      return $nb_char;
    }
    else {
      return trim($value);
    }
  }

  /**
   * Attach counter to element.
   *
   * @param mixed $element
   *   Form element.
   * @param array $color_settings
   *   Specific color settings (summary)
   * @param int $optimal_size
   *   Specific size (summary)
   * @param bool $forced_wysiwyg
   *   Force wysiwyg status (summary)
   * @param bool $force_uuid
   *   Force generating UUID (summary)
   */
  public function makeAttachement(&$element, ?array $color_settings = NULL, $optimal_size = NULL, $forced_wysiwyg = NULL, $force_uuid = FALSE) {

    $is_wysiwyg = ($forced_wysiwyg === NULL) ? $this->isWysiwyg : $forced_wysiwyg;
    $fieldMaxSize = ($this->isLong == TRUE) ? 1000000 : 255;

    if ($color_settings === NULL) {
      $color_settings = ($this->getSetting('color_settings') != $this->emptyConf) ? $this->getSetting('color_settings') : [];
    }
    if ($optimal_size === NULL) {
      $optimal_size = $this->getSetting('optimal_size') ?? $fieldMaxSize;
    }

    if ($color_settings != []) {

      $data = [];

      foreach ($color_settings as $item) {

        if ($item['low'] == $this->t('Empty')->__toString() || $item['low'] == 0) {
          $low = 0;
        }
        else {
          $low = (int) $this->getNbChar($item['low'], $fieldMaxSize);
        }

        if ($item['max'] == $this->t('Field Max size')->__toString() || $item['max'] == -1) {
          $max = $this->isLong === TRUE ? 1000000 : (int) $fieldMaxSize;
        }
        else {
          $max = (int) $this->getNbChar($item['max'], $fieldMaxSize, TRUE);
        }

        $data[] = [
          'color' => $item['color'],
          'low' => $low,
          'max' => $max,
        ];
      }

      if ($this->fieldDefinition instanceof FieldConfig && !$force_uuid) {
        $uuid = $this->fieldDefinition->get('uuid');
      }
      else {
        $uuid = \Drupal::service('uuid')->generate();
      }

      $incochars = '<span class="incochars"></span>';
      if ($this->isLong !== TRUE) {
        $incochars .= '/' . 255 . ' ' . $this->t('max characters');
        $incooptim = '<span class="incooptim">' . $this->t('The optimal size is @nb characters.', ['@nb' => $optimal_size]) . '</span>';
      }
      else {
        $incochars .= '/' . $optimal_size . ' ' . $this->t('characters (recommended value)');
        $incooptim = '';
      }

      $incoerror = '<span class="incoerror">' . $this->t('Your text is too long and not optimize for rendering.') . '</span>';

      $attachement = [
        '#field_suffix' => '<div class="counter" id="' . $uuid . '">' . $incochars . $incooptim . $incoerror . '</div>',
        '#attributes' => [
          'class' => ($is_wysiwyg === TRUE) ? ['wysiwyg-counter'] : ['counter'],
        ],
        '#attached' => [
          'library' => ['colored_field_counter/colored-counter-cplx'],
          'drupalSettings' => [
            'cplx_counter' => [
              $uuid => $data,
            ],
          ],
        ],
      ];

      $element = array_merge_recursive($element, $attachement);

    }

  }

}
