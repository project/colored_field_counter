<?php

namespace Drupal\colored_field_counter\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\link\Plugin\Field\FieldWidget\LinkWidget;

/**
 * Complex colored widget for links.
 *
 * @FieldWidget(
 *   id = "cplx_link_title",
 *   label = @Translation("Link title with colored counter (cplx)"),
 *   field_types = {
 *     "link"
 *   }
 * )
 */
class CplxLinkWidget extends LinkWidget {

  // Import Trait.
  use BaseCplxTrait;

  /**
   * Field type is textarea ?
   *
   * @var bool
   */
  protected $isLong = FALSE;
  /**
   * Field type is wysiwyg ?
   *
   * @var bool
   */
  protected $isWysiwyg = FALSE;


  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    if ($element['title']) {

      $this->makeAttachement($element['title']);

      return $element;
    }
  }

}
