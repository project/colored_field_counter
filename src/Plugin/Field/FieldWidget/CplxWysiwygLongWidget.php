<?php

namespace Drupal\colored_field_counter\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

use Drupal\text\Plugin\Field\FieldWidget\TextareaWidget;

/**
 * Plugin implementation of the 'text_textarea_with_counter' widget.
 *
 * @FieldWidget(
 *   id = "cplx_wysiwyg",
 *   label = @Translation("Formatted Text area with colored counter (cplx)"),
 *   field_types = {
 *     "text_long"
 *   }
 * )
 */
class CplxWysiwygLongWidget extends TextareaWidget {

  /**
   * Field type is textarea ?
   *
   * @var bool
   */
  protected $isLong = TRUE;
  /**
   * Field type is wysiwyg ?
   *
   * @var bool
   */
  protected $isWysiwyg = TRUE;

  use BaseCplxTrait;

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $this->makeAttachement($element);

    return $element;
  }

}
