<?php

namespace Drupal\colored_field_counter\Plugin\Field\FieldWidget;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\field\Entity\FieldConfig;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Trait For basic widget.
 *
 * @var Trait
 */
trait BaseSimpleTrait {

  /**
   * UUid service.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $uuidService;

  /**
   * Is text field a wysiwyg.
   *
   * @var bool
   */
  protected $isWysiwyg = FALSE;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, UuidInterface $uuid_service) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->uuidService = $uuid_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('uuid')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $defaults = parent::defaultSettings();
    $defaults += [
      'char_reco' => 60,
      'char_margin_min' => 10,
      'char_margin_max' => 10,
    ];
    return $defaults;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);
    $fieldSettings = $this->getFieldSettings();

    $element['char_reco'] = [
      '#type' => 'number',
      // Min 1 because required field.
      '#min' => 1,
      '#title' => $this->t('Number of recommended characters'),
      '#default_value' => empty($this->getSetting('char_reco')) ? '60' : $this->getSetting('char_reco'),
      '#description' => $this->t("Change the counter's color to red after this number"),
      '#required' => TRUE,
    ];

    if (array_key_exists('max_length', $fieldSettings)) {
      $element['char_reco']['#max'] = $fieldSettings['max_length'];
    }

    $element['char_margin_min'] = [
      '#type' => 'number',
      '#min' => 0,
      '#max' => 100,
      '#title' => $this->t('Lower margin (percentage)'),
      '#default_value' => $this->getSetting('char_margin_min') ?? 0,
      '#description' => $this->t("Leave 0 to use the default value ​​defined in the default widget settings."),
    ];
    $element['char_margin_max'] = [
      '#type' => 'number',
      '#min' => 0,
      '#max' => 100,
      '#title' => $this->t('Lock input at more than (percentage)'),
      '#default_value' => $this->getSetting('char_margin_max') ?? 0,
      '#description' => $this->t("Leave 0 to use the default value ​​defined in the default widget settings."),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $summary[] = $this->t('Number of recommended characters: @reco', ['@reco' => $this->getSetting('char_reco') ?? 60]);

    $min = $this->getSetting('char_margin_min') ?? 10;
    $max = $this->getSetting('char_margin_max') ?? 10;

    $summary[] = $this->t('Lower margin: - @min%', ['@min' => $min]);
    $summary[] = $this->t('Upper margin: + @max%', ['@max' => $max]);

    return $summary;
  }

  /**
   * Attach counter to element.
   *
   * @param mixed $element
   *   Form element.
   * @param bool $forced_wysiwyg
   *   Force wysiwyg status (summary)
   * @param array $forced_settings
   *   Specific color settings (summary)
   * @param bool $force_uuid
   *   Force generating UUID (summary)
   */
  public function makeAttachement(&$element, $forced_wysiwyg = NULL, array $forced_settings = [], $force_uuid = FALSE) {

    $is_wysiwyg = ($forced_wysiwyg === NULL) ? $this->isWysiwyg : $forced_wysiwyg;

    if ($forced_settings == []) {
      $min = empty($this->getSetting('char_margin_min')) ? 10 : $this->getSetting('char_margin_min');
      $max = empty($this->getSetting('char_margin_max')) ? 10 : $this->getSetting('char_margin_max');
      $reco = (int) $this->getSetting('char_reco') ?? 60;
    }
    else {
      $min = $forced_settings['char_margin_min'] ?? 10;
      $max = $forced_settings['char_margin_max'] ?? 10;
      $reco = $forced_settings['char_reco'] ?? 60;
    }

    // Orange = reco - % margin min.
    $orange = ($reco * (100 - (int) $min)) / 100;
    // Red = reco.
    $red = $reco;
    // Lock = red + % margin max.
    $lock = ($reco * (100 + (int) $max)) / 100;
    $element["#maxlength"] = (int) $lock;

    $field_definition = $this->fieldDefinition;
    if ($field_definition instanceof FieldConfig && !$force_uuid) {
      $uuid = $this->fieldDefinition->get('uuid');
    }
    else {
      $uuid = $this->uuidService->generate();
    }

    $incochars = '<span class="incochars"></span>/' . $reco . ' ' . $this->t('characters (recommended value)');
    $incoerror = '<span class="incoerror">' . $this->t('Your text is too long and not optimize for rendering.') . '</span>';

    $element = array_merge_recursive($element, [
      '#field_suffix' => '<div class="counter" id="' . $uuid . '">' . $incochars . $incoerror . '</div>',
      '#attributes' => [
        'class' => ($is_wysiwyg === TRUE) ? ['wysiwyg-counter'] : ['counter'],
      ],
      '#attached' => [
        'library' => ['colored_field_counter/simple-counter'],
        'drupalSettings' => [
          'counter' => [
            $uuid => [
              'orange' => $orange,
              'red' => $red,
            ],
          ],
        ],
      ],
    ]);
  }

}
