<?php

namespace Drupal\colored_field_counter\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

use Drupal\text\Plugin\Field\FieldWidget\TextareaWithSummaryWidget;

/**
 * Plugin implementation of the 'text_textarea_with_counter' widget.
 *
 * @FieldWidget(
 *   id = "cplx_wysiwyg_summary",
 *   label = @Translation("Text area with a summary with colored counter (cplx)"),
 *   field_types = {
 *     "text_with_summary"
 *   }
 * )
 */
class CplxWysiwygSummaryWidget extends TextareaWithSummaryWidget {

  use BaseCplxTrait;

  /**
   * Field type is textarea ?
   *
   * @var bool
   */
  protected $isLong = TRUE;
  /**
   * Field type is wysiwyg ?
   *
   * @var bool
   */
  protected $isWysiwyg = TRUE;

  /**
   * Field group.
   *
   * @var array
   */
  private $groups = [
    'summary_wrapper' => 'Summary',
    'full_wrapper' => 'Main content',
  ];

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'summary_wrapper' => [
        'color_settings' => [
          [
            'color' => '',
            'low' => '',
            'max' => '',
          ],
        ],
        'optimal_size' => '',
      ],
      'full_wrapper' => [
        'color_settings' => [
          [
            'color' => '',
            'low' => '',
            'max' => '',
          ],
        ],
        'optimal_size' => '',
      ],
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    $element = parent::settingsForm($form, $form_state);

    $element['notice_wrapper'] = [
      '#type' => 'details',
      '#title' => $this->t('How to configure'),
    ];
    $element['notice_wrapper']['notice'] = [
      '#type' => 'processed_text',
      '#text' => $this->t("Limits can have two forms : \n - numerics : Absolut number of characters. \n Exemple : '50' means the limit are 50 characters\n - percentages : Relative limit to the Field max size (or optimal size for for long texts)  \n Exemple : for a text field limited to 100 characters, '10%' means the limit are to 90chararcters (Max - limit)"),
    ];

    foreach ($this->groups as $group => $label) {

      if ($group == 'summary_wrapper' && $this->fieldDefinition->getSetting('display_summary') === FALSE) {
        continue;
      }

      if (!$this->getSetting($group)) {
        $settings = [];
        $optimal_size = 255;
      }
      else {
        $settings = $this->getSetting($group);
        $settings = $settings['color_settings'];
        $optimal_size = ($settings['optimal_size'] == NULL) ? 255 : $settings['optimal_size'];
      }

      $element[$group] = [
        '#type' => 'details',
        '#title' => $this->t('@group settings', ['@group' => $label]),
      ];

      $element[$group]['optimal_size'] = [
        '#type' => 'textfield',
        '#title' => $this->t('@group optimal size', ['@group' => $label]),
        '#default_value' => $optimal_size ?? "",
        '#description' => $this->t('The size (in number of characters) for the optimal render'),
      ];
      $table = $this->buildTable($settings);
      $element[$group]['color_settings'] = $table;

    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    foreach ($this->groups as $group => $label) {

      if ($group == 'summary_wrapper' && $this->fieldDefinition->getSetting('display_summary') === FALSE) {
        continue;
      }

      $data = $this->getSetting($group);
      $settings = $data['color_settings'];
      if ($settings == $this->emptyConf || $settings == NULL) {
        $summary[] = $this->t('The widget is not configured for @group.', ['@group' => $label]);
      }
      else {

        $optimal_size = $data['optimal_size'];
        $max_size = $this->getMaxSize();
        $summary[] = $this->t('for @group', ['@group' => $label]);
        $summary[] = ' ' . $this->t('- The optimal size is @optim characters', ['@optim' => $optimal_size]);
        $summary[] = ' ' . $this->t('- Color settings');
        foreach ($settings as $values) {

          $summary[] = $this->formatLine($values, $max_size, 2);

        }
      }
    }
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $max_char = $this->getMaxSize();

    // There is a summary.
    if ($this->fieldDefinition->getSetting('display_summary') === TRUE && isset($element['summary'])) {

      $summary_settings = $this->getSetting('summary_wrapper');
      $color_settings = ($summary_settings['color_settings'] != $this->emptyConf) ? $summary_settings['color_settings'] : [];
      $optimal_size = ($summary_settings['optimal_size'] != $max_char) ? $summary_settings['optimal_size'] : $max_char;

      $this->makeAttachement($element['summary'], $color_settings, $optimal_size, FALSE, TRUE);
    }

    // For main.
    $full_settings = $this->getSetting('full_wrapper');
    $color_settings = ($full_settings['color_settings'] != $this->emptyConf) ? $full_settings['color_settings'] : [];
    $optimal_size = ($full_settings['optimal_size'] != $max_char) ? $full_settings['optimal_size'] : $max_char;

    $this->makeAttachement($element, $color_settings, $optimal_size);

    return $element;
  }

}
