<?php

namespace Drupal\colored_field_counter\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\link\Plugin\Field\FieldWidget\LinkWidget;

/**
 * Class SimpleLinkWidget.
 *
 * Provides custom widget for textfield.
 * This widget add a counter below each textfield.
 * This counter informs contributors of recommended text sizes.
 * These recommendations are configurable for each textfield with this widget.
 *
 * @FieldWidget(
 *   id = "simple_link_title",
 *   label = @Translation("Link with colored counter"),
 *   field_types = {
 *     "link"
 *   }
 * )
 */
class SimpleLinkWidget extends LinkWidget {

  use BaseSimpleTrait;

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state, $key = 'title') {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    if ($element['title']) {

      $this->makeAttachement($element['title']);
    }
    return $element;

  }

}
