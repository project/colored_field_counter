<?php

namespace Drupal\colored_field_counter\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

use Drupal\text\Plugin\Field\FieldWidget\TextareaWithSummaryWidget;

/**
 * Plugin implementation of the 'text_textarea_with_counter' widget.
 *
 * @FieldWidget(
 *   id = "simple_wysiwyg_summary",
 *   label = @Translation("Text area with a summary with colored counter"),
 *   field_types = {
 *     "text_with_summary"
 *   }
 * )
 */
class SimpleWysiwygSummaryWidget extends TextareaWithSummaryWidget {

  use BaseSimpleTrait;

  /**
   * Field group.
   *
   * @var array
   */
  private $groups = [
    'summary_wrapper' => 'Summary',
    'full_wrapper' => 'Main content',
  ];

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'summary_wrapper' => [
        'char_reco' => 255,
        'char_margin_min' => 10,
        'char_margin_max' => 10,
      ],
      'full_wrapper' => [
        'char_reco' => 255,
        'char_margin_min' => 10,
        'char_margin_max' => 10,
      ],
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    $element = parent::settingsForm($form, $form_state);
    foreach ($this->groups as $group => $label) {

      if ($group == 'summary_wrapper' && $this->fieldDefinition->getSetting('display_summary') === FALSE) {
        continue;
      }

      $element[$group] = [
        '#type' => 'details',
        '#title' => $this->t('@group settings', ['@group' => $label]),
      ];

      $setting = $this->getSetting($group);

      $element[$group]['char_reco'] = [
        '#type' => 'number',
        // Min 1 because required field.
        '#min' => 1,
        '#title' => $this->t('Number of recommended characters'),
        '#default_value' => empty($setting['char_reco']) ? '60' : $setting['char_reco'],
        '#description' => $this->t("Change the counter's color to red after this number"),
        '#required' => TRUE,
      ];

      $element[$group]['char_margin_min'] = [
        '#type' => 'number',
        '#min' => 0,
        '#max' => 100,
        '#title' => $this->t('Lower margin (percentage)'),
        '#default_value' => $setting['char_margin_min'] ?? 0,
        '#description' => $this->t("Leave 0 to use the default value ​​defined in the default widget settings."),
      ];
      $element[$group]['char_margin_max'] = [
        '#type' => 'number',
        '#min' => 0,
        '#max' => 100,
        '#title' => $this->t('Lock input at more than (percentage)'),
        '#default_value' => $setting['char_margin_max'] ?? 0,
        '#description' => $this->t("Leave 0 to use the default value ​​defined in the default widget settings."),
      ];

    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    foreach ($this->groups as $group => $label) {

      if ($group == 'summary_wrapper' && $this->fieldDefinition->getSetting('display_summary') === FALSE) {
        continue;
      }

      $setting = $this->getSetting($group);

      $summary[] = $this->t('for @group', ['@group' => $label]);

      $summary[] = ' - ' . $this->t('Number of recommended characters: @reco', ['@reco' => $setting['char_reco'] ?? 255]);

      $min = $setting['char_margin_min'] ?? 10;
      $max = $setting['char_margin_max'] ?? 10;

      $summary[] = ' - ' . $this->t('Lower margin: - @min%', ['@min' => $min]);
      $summary[] = ' - ' . $this->t('Upper margin: + @max%', ['@max' => $max]);
    }
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    // There is a summary.
    if ($this->fieldDefinition->getSetting('display_summary') === TRUE && isset($element['summary'])) {

      $full_settings = $this->getSetting('summary_wrapper');

      $this->makeAttachement($element['summary'], FALSE, $full_settings, TRUE);
    }

    // For main.
    $full_settings = $this->getSetting('full_wrapper');

    $this->makeAttachement($element, TRUE, $full_settings);

    return $element;
  }

}
