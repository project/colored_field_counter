<?php

namespace Drupal\colored_field_counter\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

use Drupal\text\Plugin\Field\FieldWidget\TextareaWidget;

/**
 * Plugin implementation of the 'text_textarea_with_counter' widget.
 *
 * @FieldWidget(
 *   id = "simple_wysiwyg",
 *   label = @Translation("Formatted Text area with colored counter"),
 *   field_types = {
 *     "text_long"
 *   }
 * )
 */
class SimpleWysiwygLongWidget extends TextareaWidget {

  use BaseSimpleTrait;

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $this->makeAttachement($element);

    return $element;
  }

}
