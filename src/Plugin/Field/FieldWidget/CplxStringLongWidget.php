<?php

namespace Drupal\colored_field_counter\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

use Drupal\Core\Field\Plugin\Field\FieldWidget\StringTextareaWidget;

/**
 * Complex colored widget for long text (plain).
 *
 * @package Drupal\colored_field_counter\Plugin\Field\FieldWidget
 *
 * @FieldWidget(
 *   id = "cplx_textfield_long",
 *   label = @Translation("Text area with colored counter (cplx)"),
 *   field_types = {
 *     "string_long"
 *   }
 * )
 */
class CplxStringLongWidget extends StringTextareaWidget {

  /**
   * Field type is textarea ?
   *
   * @var bool
   */
  protected $isLong = TRUE;
  /**
   * Field type is wysiwyg ?
   *
   * @var bool
   */
  protected $isWysiwyg = FALSE;
  /**
   * Field type is textarea ?
   *
   * @var bool
   */
  protected $fieldMaxSize = -1;

  use BaseCplxTrait;

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $this->makeAttachement($element['value']);

    return $element;
  }

}
