<?php

namespace Drupal\colored_field_counter\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\StringTextfieldWidget;

/**
 * Complex colored widget for textfield.
 *
 * @FieldWidget(
 *   id = "cplx_textfield",
 *   label = @Translation("Textfield with colored counter (cplx)"),
 *   field_types = {
 *     "string",
 *     "text"
 *   }
 * )
 */
class CplxStringWidget extends StringTextfieldWidget {

  /**
   * Field type is textarea ?
   *
   * @var bool
   */
  protected $isLong = FALSE;
  /**
   * Field type is wysiwyg ?
   *
   * @var bool
   */
  protected $isWysiwyg = FALSE;

  use BaseCplxTrait;

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $this->makeAttachement($element['value']);

    return $element;
  }

}
