(function ($, Drupal, drupalSettings) {

  'use strict';
  var wysiwyg_binded = [];

  Drupal.behaviors.simple_counter = {
    attach: function (context, settings) {

        // Browse all fields when the page is loaded.
        $("div.counter, div.wysiwyg-counter", context).each(function () {
          var id = $(this).attr('id');

          // Don't process if no ID.
          if(undefined == id) {
            return;
          }

          var input = $(this).parent().prev();

          // If not an input, get textarea.
          if (input.prop('nodeName') !== 'INPUT') {
            input = input.children();
          }

          var selector = getSelector(input);
          var nbchar = input.val().replace(/(<([^>]+)>)/gi, "").length + (input.val().match(/\n/g) || '').length;

          // Update counter.
          color(id, selector, nbchar);
        });

        // At keyup on a compatible field.
        $('input.counter, textarea.counter', context).unbind('keyup').on('keyup',function (e) {
          if(isAltKey(e.keyCode)) { return;  }
          var selector = getSelector($(this));
          var id = selector.next().children('.counter').attr('id');
          var nbchar = $(this).val().replace(/(<([^>]+)>)/gi, "").length + ($(this).val().match(/\n/g) || '').length;

          color(id, selector, nbchar);
        });

        // CKEDITOR 4
        if (window.hasOwnProperty("CKEDITOR")) {
          // Wait until the editor is loaded.
          CKEDITOR.on('instanceReady', function () {
              // Check all CKEDITOR with counter
              $('textarea.wysiwyg-counter', context).each(function () {
                  var fieldID = $(this).attr('id');

                  // on key up
                  if(!wysiwyg_binded[fieldID]) {
                      wysiwyg_binded[fieldID] = true;

                      CKEDITOR.instances[fieldID].on("key", function (e) {
                          if(isAltKey(e.data.keyCode)) { return;
                          }
                          var editor = this;
                          // Wait CKEDITOR "finish it update"
                          window.setTimeout(function () {
                              var editor_id = editor.id;
                              var selector = $('.' + editor_id).parent();
                              var id = selector.next().children('.counter').attr('id');
                              var text = $.trim(editor.getData());
                              var nbchar = text.replace(/(<([^>]+)>)/gi, "").length;
                              // Update counter.
                              color(id, selector, nbchar);
                          });
                      });
                  }
              });
          });
      };

      // CKEDITOR 5
      $('div.wysiwyg-counter .ck-content.ck-editor__editable', context).each(function () {

        var selector = $(this).parent().parent().parent();
        var id = selector.next().children('.counter').attr('id');

        $(this).unbind('keyup').on('keyup',function (e) {
          if(isAltKey(e.keyCode)) { return;  }
          let text = $(this).html();
          let nbchar = text.replace(/(<([^>]+)>)/gi, "").length;

          color(id, selector, nbchar);
        });
      });

        // Manage colors and error message.
        function color(id, selector, nbchar) {
          var orange = settings['counter'][id]['orange'];
          var red = settings['counter'][id]['red'];
          var incochar = selector.next().children('.counter').children('.incochars');
          var incoerror = selector.next().children('.counter').children('.incoerror');

          incochar.text(nbchar);

          if (nbchar > orange) {
            incochar.parent().css("color", "orange");
            incoerror.css("display", "none");

            if (nbchar > red) {
              incochar.parent().css("color", "red");
              incoerror.css("display", "block");
            }
          }
          // Executed only when no margin has been entered.
          else if ((nbchar == orange) && (nbchar == red)) {
            incochar.parent().css("color", "red");
            incoerror.css("display", "block");
          }
          else {
            incoerror.css("display", "none");
            incochar.parent().css("color", "#333");
            incochar.parent().css("color", "green");
          }
        }

        // Return the correct selector depending on the type of field (textfield or textarea).
        function getSelector(input) {
          var selector = input;

          if (input.prop('nodeName') === 'TEXTAREA') {
            selector = input.parent();
          }

          return selector;
        }

        function isAltKey(keycode) {
          if(keycode == 46) {
            return false;
          }
          if(keycode >= 32 && keycode <= 255) {
            return false;
          }
          return true;
        }

    }
  };

})(jQuery, Drupal, drupalSettings);
